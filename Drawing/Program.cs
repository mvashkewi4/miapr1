﻿using library;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using static library.K;

namespace Drawing
{
    static class Program
    {
        const int size = 800;
        const int count = 10000;
        static (IList<Point2D> vectors, IEnumerable<IGrouping<int, int>> groups) result = Task(count, size, 10);
        static Image image = GetImage(result.vectors, result.groups);

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new Form1
            {
                Text = "МиАПР"
            };
            form.Paint += DrawGraphics;

            Application.Run(form);
        }

        private static void DrawGraphics(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(image, new Point(0, 0));
        }

        private static Color Colorize(this int index)
        {
            int r = (index * 40) % 255;
            int g = (index * 60) % 255;
            int b = (index * 80) % 255;
            return Color.FromArgb(r, g, b);
        }

        private static Image GetImage(IList<Point2D> vectors, IEnumerable<IGrouping<int, int>> groups)
        {
            var image = new Bitmap(size, size);
            var drawing = Graphics.FromImage(image);
            drawing.Clear(Color.White);

            foreach (var group in groups)
            {
                var color = group.Key.Colorize();
                var brush = new SolidBrush(color);
                foreach (var point in group)
                {
                    const int radius = 3;
                    var r = new Rectangle((int)vectors[point].x - radius, (int)vectors[point].y - radius, radius * 2, radius * 2);
                    drawing.FillEllipse(brush, r);
                }
                const float rectSize = 5.0f;
                drawing.FillRectangle(Brushes.Red, new RectangleF((float)(vectors[group.Key].x - rectSize), (float)(vectors[group.Key].y - rectSize), rectSize * 2.0f, rectSize * 2.0f));
            }

            return image;
        }
    }
}
