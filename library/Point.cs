﻿using System;
using System.Collections.Generic;
using System.Text;

namespace library
{
    public struct Point2D
    {
        public double x { get; }
        public double y { get; }

        public Point2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double Norm
        {
            get => Math.Sqrt(x * x + y * y);
        }

        public double Distance(Point2D other) => Math.Sqrt(Sqr(this.x - other.x) + Sqr(this.y - other.y));

        public static double Sqr(double value) => value * value;
    }
}
