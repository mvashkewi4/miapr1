﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace library
{
    public static class K
    {
        private static Random random = new Random();

        public static IList<Point2D> GenerateVectors(int count, int maxCoordinateValue)
        {
            var vectors = new Point2D[count];
            for (int i = 0; i < vectors.Length; ++i)
            {
                vectors[i] = new Point2D(NextDouble(maxCoordinateValue), NextDouble(maxCoordinateValue));
            }

            return vectors;
        }

        public static IList<int> GetInitialKernelIndexes(this IList<Point2D> vectors, int count)
        {
            List<int> results = new List<int>(count);
            while (results.Count != count)
            {
                int nextIndex = random.Next(vectors.Count);
                if (!results.Contains(nextIndex))
                {
                    results.Add(nextIndex);
                }
            }

            return results;
        }

        public static IEnumerable<IGrouping<int, int>> GroupVectors(this IList<Point2D> vectors, IList<int> kernelIndexes)
        {
            return vectors
                .Select((v, i) => (v, i))
                .GroupBy(pair => pair.v.GetGroupKernelIndex(vectors, kernelIndexes),
                         pair => pair.i);
        }

        public static IList<int> GetKernels(this IList<Point2D> vectors, IEnumerable<IGrouping<int, int>> groups)
        {
            var newKernels = new List<int>(groups.Count());

            foreach (var group in groups)
            {
                newKernels.Add(group.Select(point =>
                (point, group.Aggregate(0.0, (sum, pointIndex) => sum + Sqr(vectors[point].Distance(vectors[pointIndex])))))
                    .MinBy(pair => pair.Item2).point);
            }

            return newKernels;
        }

        public static (IList<Point2D> vectors, IEnumerable<IGrouping<int, int>> groups) Task(int count, int maxCoord, int groupsCount)
        {
            IList<Point2D> vectors = GenerateVectors(count, maxCoord);
            IList<int> kernels = vectors.GetInitialKernelIndexes(groupsCount);
            var groups = vectors.GroupVectors(kernels);
            IList<int> newKernels = vectors.GetKernels(groups);
            while (!newKernels.All(newKernel => kernels.Contains(newKernel)))
            {
                kernels = newKernels;
                groups = vectors.GroupVectors(kernels);
                newKernels = vectors.GetKernels(groups);
            }

            return (vectors, groups);
        }

        private static double Sqr(double v) => v * v;

        private static int GetGroupKernelIndex(this Point2D vector, IList<Point2D> vectors, IList<int> kernelIndexes)
        {
            return kernelIndexes.MinBy(index => vector.Distance(vectors[index]));
        }

        private static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var comparer = Comparer<TKey>.Default;

            if (!source.Any())
            {
                throw new ArgumentException("empty");
            }

            if (!source.Skip(1).Any())
            {
                return source.First();
            }

            TSource minValue = source.First();
            TKey minKey = keySelector(minValue);

            foreach (var value in source)
            {
                var key = keySelector(value);
                if (comparer.Compare(key, minKey) < 0)
                {
                    minValue = value;
                    minKey = key;
                }
            }

            return minValue;
        }

        private static double NextDouble(int maxValue) => random.NextDouble() * maxValue;
    }
}
